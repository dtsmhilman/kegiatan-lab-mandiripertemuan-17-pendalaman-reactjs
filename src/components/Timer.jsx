import React from 'react'

class Timer extends React.Component{
    timer = 0
    constructor(props){
        super(props)
        this.state = ({
            secondNumber: 0
        })
        this.startTimer = this.startTimer.bind(this)
        this.stopTimer = this.stopTimer.bind(this)
    }

    startTimer(){
        this.timer = setInterval(()=>{
            this.setState({
                secondNumber: this.state.secondNumber + 1
            })
        }, 1000)

    }

    stopTimer(){
        this.setState({
            secondNumber: this.state.secondNumber
        })
        clearInterval(this.timer)
    }

    render(){
        return (
        <div>
            <h3>Klik Start Untuk Menjalankan Timer /detik</h3>
            <button className={this.startTimer ? 'btn btn-success' : 'btn btn-danger'} onClick={this.startTimer} >Start  </button>
            <button className={this.stopTimer ? 'btn btn-danger' : 'btn btn-success'} onClick={this.stopTimer}>  Stop</button>
            <h2>{this.state.secondNumber}</h2>
        </div>
        )
    }

}
export default Timer;

// class Timer extends React.Component {
//     constructor(props) {
//     super(props);
//     this.state = {date: new Date(true)};

//     }

//         stopTimer(){
//         this.setState({
//             time: this.state.date
//         })
//         clearInterval(this.time)

//     }

//     componentDidMount() {
//     this.timerID = setInterval(
//         () => this.tick(),
//         1000
//     );
//     }

//     componentWillUnmount() {
//     clearInterval(this.timerID);
//     }

//     tick() {
//     this.setState({
//         date: new Date()
//     });
//     }

//     render() {
//         this.stopTimer = this.stopTimer.bind(this)
//     return (
//         <div>
//             <button className={this.stopTimer ? 'btn btn-danger' : 'btn btn-success'} onClick={this.stopTimer}>  Stop</button>
//         <h2>{this.state.date.toLocaleTimeString(0.00)}.</h2>
//         </div>
//     );
//     }
// }

