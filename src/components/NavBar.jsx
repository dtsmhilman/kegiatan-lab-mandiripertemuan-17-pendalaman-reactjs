import React from 'react'
import { Link, NavLink} from 'react-router-dom'

class NavBar extends React.Component{
    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <a className="navbar-brand">Online Store</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div className="navbar-nav">
                    {/* <Link to="/" className="nav-link">Home</Link>
                    <Link to="/about" className="nav-link">About</Link>
                    <Link to="/home" className="nav-link">home</Link> */}
                    <a className="nav-link active" href="/">Home <span className="sr-only">(current)</span></a>
                    <a className="nav-link" href="/about">About</a>
                    <a className="nav-link" href="/login">Login</a>
                </div>
                </div>
            </nav>
        )
    }
}

export default NavBar;