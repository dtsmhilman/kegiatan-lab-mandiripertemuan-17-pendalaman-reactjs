import React from 'react'

class Lampu extends React.Component{
    constructor(props){
        super(props)
        this.state = {isLampOn: true}
        this.saklarKlik = this.saklarKlik.bind(this)

    }

    saklarKlik(){
        this.setState(state=>({
            isLampOn: !state.isLampOn
        }))
    }

    // componentDidUpdate(){
    //     document.getElementById()
    // }


    render(){
        return(
            <div>
                <button className= {this.state.isLamp ? 'btn btn-success' : 'btn btn-danger'} onClick={this.saklarKlik}>
                {this.state.isLampOn ? 'Hidup' : 'Mati'}
                </button>
            </div>
        )
    }

}

export default Lampu;