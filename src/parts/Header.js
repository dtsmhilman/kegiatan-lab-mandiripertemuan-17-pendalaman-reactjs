import React from 'react';
import logo from '../logo.svg';
import { Navbar } from 'react-bootstrap';
import { Nav } from 'react-bootstrap';
import { Form } from 'react-bootstrap';
import { FormControl } from 'react-bootstrap';
import { NavDropdown } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
// import 'bootstrap/dist/css/bootstrap.min.css';


export default function Header(props) {
    return (
        <navbar bg='dark' expand='lg'>
            <Navbar.Brand href='#home'><b>Online Store</b></Navbar.Brand>
            <Navbar.Toggle aria-controls='basic-navbar-nav' />
            <Navbar.Collapse id='basic-navbar-nav'>
                <Nav className='mr-auto'>
                <Nav.Link href='#home'>Home</Nav.Link>
                <Nav.Link href='#link'>Link</Nav.Link>
                <NavDropdown title='Dropdown' id='basic-nav-dropdown'>
                    <NavDropdown.Item href='#action/3.1'>Action</NavDropdown.Item>
                    <NavDropdown.Item href='#action/3.2'>
                    Another action
                    </NavDropdown.Item>
                    <NavDropdown.Item href='#action/3.3'>Something</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href='#action/3.4'>
                    Separated link
                    </NavDropdown.Item>
                </NavDropdown>
                </Nav>
                <Form inline>
                <FormControl type='text' placeholder='Search' className='mr-sm-2' />
                <Button variant='outline-success'>Search</Button>
                </Form>
            </Navbar.Collapse>
        </navbar>
    );
}