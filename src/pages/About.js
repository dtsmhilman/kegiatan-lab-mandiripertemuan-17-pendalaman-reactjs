import React, { Component } from 'react';
import Header from '../parts/Header';
export default class About extends Component {
    render() {
        return (
        <div class="container">
            <Header></Header>
            <h3>Ini Halaman About</h3>
            <div class="justify-content-center">
            <div class="row">
                <div class="col-sm-6">
                    <div class="card shadow-lg">
                    <div class="card-body">
                        <h5 class="card-title">Special title treatment</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card shadow-lg">
                    <div class="card-body">
                        <h5 class="card-title">Special title treatment</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                    </div>
                </div>
                </div>
            <a href="/about">Go to</a>
            </div>
        </div>
        );
    }
}