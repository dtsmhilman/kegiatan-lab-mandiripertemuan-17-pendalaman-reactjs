import React, { Component } from 'react';
import Header from '../parts/Header';
export default class About extends Component {
    render() {
        return (
        <div class="container">
            <h3 class="my-3">List Barang</h3>
            <div class="justify-content-center mb-2">
            <div class="row">
                <div class="col-sm-6">
                    <div class="card shadow-lg">
                    <div class="card-body">
                    <img class="card-img" src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/5/15/642240/642240_59c2c6a5-67e3-4398-b3c7-87f489d0184e.jpg" width="100%" alt="Card image cap"/>
                        <h5 class="card-title">Baju Pria Kaos Muslim PS-LSMSZN641 SUKSES SUAMI DOA ISTRI 4 - Putih, S</h5>
                        <p class="card-text">"Assalamu'''alaikum,

Mari lengkapi koleksi kaos muslim anda untuk menghiasi bulan Ramadhan dan hari raya Lebaran kamu. Model baju muslim Zaflan ini cocok dipakai untuk anda muslimin yang ingin tampil simple, modern tapi tetap menampilkan ciri Islami.
</p>
                        <a href="/about" class="btn btn-primary">Beli</a>
                    </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card shadow-lg">
                    <div class="card-body">
                    <img class="card-img" src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/3/25/642240/642240_77227a8c-0e8e-45a5-aa72-731f52a51e85.jpg" width="100%" alt="Card image cap"/>
                        <h5 class="card-title">Kaos Muslim Panjang Pria SN-LLMSMY328 UPDATE HATI VERSI 2 - Putih, S</h5>
                        <p class="card-text">Produk ini menggunakan material bahan katun combed 20'''s yang lembut, lentur dan sangat nyaman di pakai untuk aktivitas keseharian anda. Produk ini memiliki 5 varian ukuran dan 5 varian warna. Sebelum memesan, mohon untuk mengecek spesifikasi ukuran (Size Chart) yang kami miliki untuk disesuaikan dengan ukuran yang biasa anda gunakan, pastikan anda tidak salah dalam memilih ukuran ya.</p>
                        <a href="/about" class="btn btn-primary">Beli</a>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="justify-content-center mb-2">
            <div class="row">
                <div class="col-sm-6">
                    <div class="card shadow-lg">
                    <div class="card-body">
                    <img class="card-img" src="https://ecs7.tokopedia.net/img/cache/700/product-1/2020/7/1/4950190/4950190_9162ea36-20a1-487f-bba4-d99f33d27134_788_788" width="100%" alt="Card image cap"/>
                        <h5 class="card-title">Baju Batik Wanita Zeintin Original 4462 Toko Inu</h5>
                        <p class="card-text">Baju Batik Wanita Zeintin Original 4462 Toko Inu
ZEINTIN edisi: Promo (sale)
Baju Batik Atasan Wanita Bahan Cotton (Ready Stok)

Harga Katalog : Rp. 100.000 jual di bawah Harga Katalog.</p>
                        <a href="/about" class="btn btn-primary">Beli</a>
                    </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card shadow-lg">
                    <div class="card-body">
                    <img class="card-img" src="https://ecs7.tokopedia.net/img/cache/700/hDjmkQ/2020/10/16/8a079eb8-c566-4759-8d0a-f7b2ccbb350c.jpg" width="100%" alt="Card image cap"/>
                        <h5 class="card-title">Gantungan Baju 5 Ring Anti Karat Untuk Display Toko/Rumah</h5>
                        <p class="card-text">Description: Made of iron material, durable and not easy to rust. Ea rack contents have 5 rings on 5 different levels. Installed on the wall , the clothes rack has lovely look and elegant style. Great for hanging all kinds of things, su as handbags, coats, shirts. Perfect for being used in boutique, laundry room, bedroom, kiten, hotel, balcony.</p>
                        <a href="/about" class="btn btn-primary">Beli</a>
                    </div>
                    </div>
                </div>
                </div>
            </div>

        </div>
        );
    }
}