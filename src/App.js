import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import Home from './pages/Home';
import About from './pages/About';
import Login from './pages/Login';


function App() {
  return (
    <Router>
      <Route exact path='/' component={Home}></Route>
      <Route path='/about' component={About}></Route>
      <Route path='/login' component={Login}></Route>
    </Router>
  );
}

export default App;



//TIMER TUGAS 1 
// import React from 'react';
// import logo from './logo.svg';
// import './App.css';
// import Lampu from './components/Lampu'
// import Timer from './components/Timer'
// // import Table from './components/Table'

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//       <Timer />
//         <img src={logo} className="App-logo" alt="logo" />
//         <Lampu />
//         {/* <Table /> */}
//         {/* <button className="btn btn-danger">Tombol Bootstrap</button> */}
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;
